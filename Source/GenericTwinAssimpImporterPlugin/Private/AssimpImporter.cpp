/**

The MIT License (MIT)

Copyright (c) 2023 - present, Hamburg Port Authority


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

**/

#pragma once

#include "AssimpImporter.h"

#include "Common/Scene/Scene.h"

#include "Common/Material/GenericTwinTextureCache.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace GenericTwin {

FAssimpImporter* FAssimpImporter::Create()
{
	return new FAssimpImporter;
}

FAssimpImporter::FAssimpImporter()
{

}

FAssimpImporter::~FAssimpImporter()
{

}

FString FAssimpImporter::GetImporterName() const
{
	return FString(TEXT("AssimpImporter"));
}

GenericTwin::ScenePtr FAssimpImporter::ImportModel(const FString &PathToModel, const SModelImportConfiguration &ImportConfiguration)
{
	GenericTwin::ScenePtr scenePtr;

	Assimp::Importer* assImporter = new Assimp::Importer();

	if(assImporter)
	{
		m_ImportConfiguration = ImportConfiguration;

		uint32 importFlags = aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_FixInfacingNormals | aiProcess_GenNormals | aiProcess_MakeLeftHanded | aiProcess_GenUVCoords;
		if(m_ImportConfiguration.FlipWindingOrder)
			importFlags |= aiProcess_FlipWindingOrder;

		m_SrcScene = assImporter->ReadFile(TCHAR_TO_UTF8(*PathToModel), importFlags);

		const aiNode *srcRootNode = m_SrcScene ? m_SrcScene->mRootNode : 0;
		if(srcRootNode)
		{
			m_Scene = new GenericTwin::SScene;
			scenePtr = GenericTwin::ScenePtr(m_Scene);

			if (m_Scene)
			{
				m_TextureCache = ImportConfiguration.LoadTextures ? &(UGenericTwinTextureCache::GetInstance()) : 0;

				ImportNode(*srcRootNode, *(m_Scene->GetRootNode()), ImportConfiguration.ImportTransform);
			}
		}

		delete assImporter;
	}

	return scenePtr;
}


void FAssimpImporter::ImportNode(const struct aiNode &SrcNode, GenericTwin::SSceneNode &DstNode, const FTransform &ParentTransform)
{
	DstNode.NodeName = FString( ANSI_TO_TCHAR(SrcNode.mName.C_Str()) );
	
	const aiMatrix4x4& m = SrcNode.mTransformation;
	DstNode.LocalTransform = FTransform(FVector(m.a1, m.b1, m.c1), FVector(m.a2, m.b2, m.c2), FVector(m.a3, m.b3, m.c3), FVector(m.a4, m.b4, m.c4));
	DstNode.WorldTransform = DstNode.LocalTransform * ParentTransform;

	for(uint32 i = 0; i < SrcNode.mNumMeshes; ++i)
	{
		const uint32 srcMeshIndex = SrcNode.mMeshes[i];
		if(m_ImportedMeshes.Contains(srcMeshIndex))
		{
			DstNode.Meshes.Add(m_ImportedMeshes[srcMeshIndex]);
		}
		else
		{
			GenericTwin::SSceneMesh *ImportedMesh = ImportMesh(*(m_SrcScene->mMeshes[srcMeshIndex]));
			if(ImportedMesh)
			{
				DstNode.Meshes.Add(ImportedMesh);
				m_ImportedMeshes.Add(srcMeshIndex, ImportedMesh);
			}
		}
	}


	for(uint32 i = 0; i < SrcNode.mNumChildren; ++i)
	{
		GenericTwin::SSceneNode *childNode = m_Scene->CreateSceneNode(&DstNode);
		ImportNode(*(SrcNode.mChildren[i]), *childNode, DstNode.WorldTransform);
	}
}


GenericTwin::SSceneMesh* FAssimpImporter::ImportMesh(const struct aiMesh &SrcMesh)
{
	GenericTwin::SSceneMesh *DstMesh = m_Scene->CreateMesh(FString( ANSI_TO_TCHAR(SrcMesh.mName.C_Str())));
	
	if(DstMesh)
	{
		if(SrcMesh.HasPositions() && SrcMesh.HasFaces())
		{
			const bool hasNormals = SrcMesh.HasNormals();
			const bool hasTexCoords = SrcMesh.HasTextureCoords(0);

			DstMesh->Vertices.Reserve(SrcMesh.mNumVertices);
			if(hasNormals)
			{
				DstMesh->Normals.Reserve(SrcMesh.mNumVertices);
			}
			if(hasTexCoords)
			{
				DstMesh->UV0.Reserve(SrcMesh.mNumVertices);
			}

			for(uint32 vInd = 0; vInd < SrcMesh.mNumVertices; ++vInd)
			{
				FVector pos(SrcMesh.mVertices[vInd].x, SrcMesh.mVertices[vInd].y, SrcMesh.mVertices[vInd].z);
				DstMesh->Vertices.Add( pos );

				if(hasNormals)
				{
					FVector nrm = FVector(SrcMesh.mNormals[vInd].x, SrcMesh.mNormals[vInd].y, SrcMesh.mNormals[vInd].z);
					if(m_ImportConfiguration.FlipNormals)
						nrm *= -1.0;
					DstMesh->Normals.Add( nrm );
				}

				if(hasTexCoords)
					DstMesh->UV0.Add(FVector2D(SrcMesh.mTextureCoords[0][vInd].x, SrcMesh.mTextureCoords[0][vInd].y));
			}

			for(uint32 fInd = 0; fInd < SrcMesh.mNumFaces; ++fInd)
			{
				const aiFace &face = SrcMesh.mFaces[fInd];
				const uint32 numIndices = (face.mNumIndices / 3) * 3;
				for(uint32 iInd = 0; iInd < numIndices; iInd += 3)
				{
					DstMesh->Indices.Add(face.mIndices[iInd + 0] );
					DstMesh->Indices.Add(face.mIndices[iInd + 1] );
					DstMesh->Indices.Add(face.mIndices[iInd + 2] );
				}
			}

			DstMesh->MaterialName = ImportMaterial(SrcMesh.mMaterialIndex);
		}
	}

	return DstMesh;
}

FString FAssimpImporter::ImportMaterial(uint32 MaterialIndex)
{
	if(m_MaterialMap.Contains(MaterialIndex) == false)
	{
		const aiMaterial &aiMaterial = *(m_SrcScene->mMaterials[MaterialIndex]);

		aiString aiName;
		aiMaterial.Get(AI_MATKEY_NAME, aiName);

		FString matName( ANSI_TO_TCHAR(aiName.C_Str()) );
		GenericTwin::SSceneMaterial *sceneMaterial = m_Scene->GetOrCreateSceneMaterial(matName);
		if(sceneMaterial)
		{
			sceneMaterial->MaterialName = matName;
			aiColor3D albedo (0.f,0.f,0.f);
			if(aiMaterial.Get(AI_MATKEY_COLOR_DIFFUSE, albedo) == AI_SUCCESS)
			{
				sceneMaterial->Parameters.Add("albedo", FVector4(albedo.r, albedo.g, albedo.b, 1.0f));
			}

			aiString textureFile;
			if	(	m_TextureCache
				&&	aiMaterial.Get(AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), textureFile) == AI_SUCCESS
				)
			{
				FString textureUrl = FString(ANSI_TO_TCHAR(textureFile.C_Str()));
				const aiTexture* embeddedTexture = m_SrcScene->GetEmbeddedTexture(textureFile.C_Str());
				if(embeddedTexture)
				{
					if(m_TextureCache->AddTexture(textureUrl, embeddedTexture->pcData, embeddedTexture->mWidth))
					{
						sceneMaterial->Textures.Add(TEXT("AlbedoMap"), textureUrl);
					}
				}
				else
				{
					if(m_TextureCache->AddTexture(textureUrl))
					{
						sceneMaterial->Textures.Add(TEXT("AlbedoMap"), textureUrl);
					}
				}
			}

			m_MaterialMap.Add(MaterialIndex, matName);
		}
	}

	return m_MaterialMap[MaterialIndex];
}


}	//	namespace GenericTwin
