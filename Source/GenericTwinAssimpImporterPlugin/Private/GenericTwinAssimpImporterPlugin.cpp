// Copyright Epic Games, Inc. All Rights Reserved.

#include "GenericTwinAssimpImporterPlugin.h"

#include "AssimpImporter.h"

#include "Import/ModelImporter.h"

#define LOCTEXT_NAMESPACE "FGenericTwinAssimpImporterPluginModule"

void FGenericTwinAssimpImporterPluginModule::StartupModule()
{
	GenericTwin::ModelImporter::RegisterImporter(TEXT("fbx"), &GenericTwin::FAssimpImporter::Create);
	GenericTwin::ModelImporter::RegisterImporter(TEXT("glb"), &GenericTwin::FAssimpImporter::Create);

	GenericTwin::ModelImporter::RegisterImporter(TEXT("ifc"), &GenericTwin::FAssimpImporter::Create);
}

void FGenericTwinAssimpImporterPluginModule::ShutdownModule()
{
	GenericTwin::ModelImporter::UnregisterImporter(TEXT("fbx"));
	GenericTwin::ModelImporter::UnregisterImporter(TEXT("glb"));

	GenericTwin::ModelImporter::UnregisterImporter(TEXT("ifc"));
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FGenericTwinAssimpImporterPluginModule, GenericTwinAssimpImporterPlugin)