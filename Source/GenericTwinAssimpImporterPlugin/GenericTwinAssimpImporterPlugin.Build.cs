// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class GenericTwinAssimpImporterPlugin : ModuleRules
{
	public GenericTwinAssimpImporterPlugin(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				// ... add private dependencies that you statically link with here ...	

				"GenericTwinCore",

			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

		PublicIncludePaths.Add(Path.Combine("$(PluginDir)", "Source", "ThirdParty", "assimp", "include"));
		PublicAdditionalLibraries.Add(Path.Combine("$(PluginDir)", "Source", "ThirdParty", "assimp", "lib", "assimp-vc142-mt.lib"));
		PublicAdditionalLibraries.Add(Path.Combine("$(PluginDir)", "Source", "ThirdParty", "assimp", "lib", "zlibstatic.lib"));

	}
}
